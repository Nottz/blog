import { Component, Input, OnInit } from '@angular/core';

import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() created_at: Date;
  @Input() loveIts: number;
  @Input() index: number;
  @Input() id: number;

  constructor(private postService: PostService) {

  }

  ngOnInit() {
    this.postService.getPostsFromServer();
  }

  dislike(i) {
    this.postService.dislike(i);
  }

  like(i) {
    this.postService.like(i);
  }

  onDelete(id) {
    this.postService.delete(id);
  }
}
