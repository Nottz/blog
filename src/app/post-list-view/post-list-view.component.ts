import {Component, Input, OnInit} from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post-list-view',
  templateUrl: './post-list-view.component.html',
  styleUrls: ['./post-list-view.component.scss']
})
export class PostListViewComponent implements OnInit {

  @Input() posts: any;
  @Input() title: string;
  @Input() content: string;
  @Input() created_ad: Date;
  @Input() loveIts: number;
  @Input() index: number;
  @Input() id: number;

  constructor(private postService: PostService) { }

  ngOnInit() {
      this.postService.getPostsFromServer();
  }

  onSave() {
    this.postService.savePostsToServer();
  }


}
