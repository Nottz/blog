import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Subject} from 'rxjs/internal/Subject';



@Injectable()

export class PostService {

  postsSubject = new Subject<any[]>();

  private posts = [
    {
      id: 1,
      title: 'Le premier post',
      content: 'J`ai trouvé du contenu',
      loveIts: 0,
      created_at: new Date()
    },
    {
      id: 2,
      title: 'Le deuxième post',
      content: 'Je cherche du contenu lol',
      loveIts: 0,
      created_at: new Date()
    },
    {
      id: 3,
      title: 'Le troisième post',
      content: 'Fan de chichoule',
      loveIts: 0,
      created_at: new Date()
    }
  ];

  constructor(private httpClient: HttpClient) { }

  emitPostSubject() {
    this.postsSubject.next(this.posts.slice());
  }

  savePostsToServer() {
    this.httpClient
      .put('https://la-lopezerie.firebaseio.com/posts.json', this.posts)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getPostsFromServer() {
    this.httpClient
      .get<any[]>('https://la-lopezerie.firebaseio.com/posts.json')
      .subscribe(
        (response) => {
          this.posts = response;
          this.emitPostSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  like(i) {
    this.posts[i].loveIts++;
    this.emitPostSubject();
  }

  dislike(i) {
    this.posts[i].loveIts--;
    this.emitPostSubject();
  }

  delete(id) {
    const index = this.posts.map(function(x) { return x.id; }).indexOf(id);
    this.posts.splice(index, 1);
    this.emitPostSubject();
  }

  getPostById(id: number) {
    const post = this.posts.find(
      (s) => {
        return s.id === id;
      }
    );
    this.emitPostSubject();
    return post;
  }

  addPost(title: string, content: string) {
    const postObject = {
      id: 0,
      title: '',
      content: '',
      loveIts: 0,
      created_at: new Date()
    };
    postObject.title = title;
    postObject.content = content;
    postObject.id = this.posts[(this.posts.length - 1)].id + 1;
    this.posts.push(postObject);
    this.emitPostSubject();
  }
}
