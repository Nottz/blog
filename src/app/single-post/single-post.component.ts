import {Component, Input, OnInit} from '@angular/core';
import { PostService } from '../services/post.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {

  @Input() posts: any;
  @Input() title: string;
  @Input() content: string;
  @Input() created_ad: Date;
  @Input() loveIts: number;
  @Input() index: number;
  @Input() id: number;

  constructor(private postService: PostService, private route: ActivatedRoute) { }

    ngOnInit() {
      const id = this.route.snapshot.params['id'];
      this.title = this.postService.getPostById(+id).title;
      this.content = this.postService.getPostById(+id).content;
    }


}
